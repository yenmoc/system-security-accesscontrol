# System Security AccessControl

## What
* System.Security.AccessControl 4.6.0 [net461]

## Installation

```bash
"com.yenmoc.system-security-accesscontrol":"https://gitlab.com/yenmoc/system-security-accesscontrol"
or
npm publish --registry=http://localhost:4873
```
